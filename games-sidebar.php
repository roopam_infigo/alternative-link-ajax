<aside data-css-sidebar="sidebar">
  <form data-css-form="filter" data-js-form="filter" class="category-form">
    <fieldset data-css-form="group">
      <?php
      $genres = get_terms( array(
          'taxonomy' => 'genre',
          'hide_empty' => false,
      ) );
      ?>
      <select data-css-form="input select" id="movie-genre" name="movie-genre">
        <option>Select Games Category</option>
        <?php foreach($genres as $genre) : ?>
        <option value="<?= $genre->term_id; ?>"><?= $genre->name; ?></option>
        <?php endforeach; ?>
      </select>
    </fieldset>
    <fieldset data-css-form="group right">
      <button data-css-button="button red" class="cat-btn">Apply Filter</button>
      <input type="hidden" name="action" value="filter">
    </fieldset>
  </form>
</aside>