<?php
/************* Service custom post type ***********************/
function my_theme_game_type() {

register_post_type( 'games',
		array(
			'labels' => array(
			'name' => __('Games','eduschool'),
			'add_new' => __('Add New Games', 'eduschool'),
			'add_new_item' => __('Add New Game','eduschool'),
			'edit_item' => __('Edit Games','eduschool'),
			'new_item' => __('New Game','eduschool'),
			'all_items' => __('All Games','eduschool'),
			'view_item' => __('View Games','eduschool'),
			'search_items' => __('Search Games','eduschool'),
			'not_found' =>  __('No Games found','eduschool'),
			'not_found_in_trash' => __('No Games found in Trash','eduschool'), 
			),
		'supports' => array('title','thumbnail','editor'),
		'show_in' => true,
		'show_in_nav_menus' => false,
		'rewrite' => array('slug' => 'games'),		
		'public' => true,
		'menu_position' =>20,
		'menu_icon' => 'dashicons-admin-generic',
		)
	);
	
	  
	// Initialize New Taxonomy Labels
	$labels = array(
	'name' => _x( 'Genre', 'Genre', 'twentytwentyone' ),
	'singular_name' => _x( 'Genre Name', 'taxonomy singular name', 'twentytwentyone' ),
	'search_items' =>  __( 'Search Genre ', 'twentytwentyone' ),
	'all_items' => __( 'All Genre ', 'twentytwentyone' ),
	'parent_item' => __( 'Parent Genre ', 'twentytwentyone' ),
	'parent_item_colon' => __( 'Parent Genre:', 'twentytwentyone' ),
	'edit_item' => __( 'Edit Genre', 'twentytwentyone' ),
	'update_item' => __( 'Update Genre', 'twentytwentyone' ),
	'add_new_item' => __( 'Add News Genre ', 'twentytwentyone' ),
	'new_item_name' => __( 'New Genre Name', 'twentytwentyone' ),
	);

    // Custom taxonomy for Project Tags
    register_taxonomy('genre',array('games'), array(
		'public' => true,
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'query_var' => true,
		'menu_position' =>20,
		'rewrite' => array( 'slug' => 'genre' ),
	));
	

}
add_action( 'init', 'my_theme_game_type' );