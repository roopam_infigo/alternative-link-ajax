<?php

add_action( 'wp_ajax_nopriv_filter', 'filter_ajax' );
add_action( 'wp_ajax_filter', 'filter_ajax' );

function filter_ajax() {


$movie_genre = $_POST['movie-genre'];
$args = array(
    'post_type' => 'games',
    'posts_per_page' => -1,
    'order' => 'ASC'
  );

//genre dropdown
if(!empty($movie_genre)){
  $args['tax_query'] = array(
    array(
'taxonomy' => 'genre',
'field'    => 'term_id',
'terms'    => array($movie_genre)
    )
  );

} else { ?>
<p>no result found</p>
<?php
}

  $query = new WP_Query($args);

  if($query->have_posts()) : 
    
    $counter = 0;
    
    while($query->have_posts()) : $counter++; $query->the_post();

    $movie_id = get_the_ID();
  
    $url = get_the_permalink();
    $title = get_the_title();
    $thumb = get_the_post_thumbnail_url();
    $content = get_the_content();
    $year = get_post_meta($movie_id,'year',true);
    $rating = get_post_meta($movie_id,'rating',true);
    $runtime = get_post_meta($movie_id,'runtime',true);
    $score = get_post_meta($movie_id,'score',true);

    $taxonomy = 'genre';
 
    // Get the term IDs assigned to post.
    $post_terms = wp_get_object_terms( $movie_id, $taxonomy, array( 'fields' => 'ids' ) );
    
    // Separator between links.
    $separator = ', ';
    
    if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
    
        $term_ids = implode( ',' , $post_terms );
    
        $terms = wp_list_categories( array(
            'title_li' => '',
            'style'    => 'none',
            'echo'     => false,
            'taxonomy' => $taxonomy,
            'include'  => $term_ids
        ) );
    
        $terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
    }

?>

<div class="block-content-single">
    <div data-css-card="movie-wrapper"class="container" >
        <div data-css-card="movie-thumbnail" class= "post-image col-md-3">
        <?php $game_image = get_field("game-image");?>
                            <?php $picture = $game_image['sizes']['thumbnail'];?>
                            
                            <img src="<?php echo $picture;?>" alt=""> 
        </div>
        <div data-css-card="movie-summary" class="post-text col-md-9">
        <h2 class="site-title"><span class="number"><?= $counter; ?>. </span><a href="<?= $url;?>"><?= $title; ?></a> <span class="date">(<?= $year; ?>)</span></h2>
        <div data-css-card="movie-meta"> 
            <div class="categories">
            <?= $terms; ?>
            </div>
        </div>
        <p data-css-card="movie-score"><span class="score"><?= $score; ?></span></p>
        <?= $content; ?>
        <div class="show_more"><button  class="cat-btn"><a href="<?= $url;?>">Show Details</a></button></div>
        </div>
    </div>
    </div>
<?php endwhile;
endif; 
wp_reset_postdata(); 

die();
}
