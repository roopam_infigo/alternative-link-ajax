<?php
/*Games post type single post display page*/
get_header(); ?>
<div id="main-content" class = "main-content Alternative-list">
	<div class="block-content">
		<div class="container">
			<div class="row">
				<div class="item-single-post">
					<div class= "post-image col-md-3">
						<?php $game_image = get_field("game-image");?>
						<?php $picture = $game_image['sizes']['thumbnail'];?>
						<img src="<?php echo $picture;?>" alt=""> 
					</div> 

					<div id="post-text" class="post-text col-md-9">
						<h2 class="site-title"><?php the_title();?></h2>
						<div class="post-meta-categories">
						<?php 
						$the_categories_list = the_terms( $post->ID, 'genre',' ', ' | ', ' ' ); ?>
						
						<?php echo $the_categories_list; ?>
						</div> 

						<div class="socials inline-inside socials-colored-hover social-alt-list">
						<?php 
						$game_website_url = get_post_meta($post->ID , 'game_website_url', true);
						if(!empty( $game_website_url)){ ?>
						<a href="<?php echo $game_website_url; ?>" class ="game_website-url">Official Website</a>
						<?php }
						$facebook = get_post_meta($post->ID,'facebook_profile_url', true);
						if(!empty($facebook)){?>
					   <a href="<?php echo $facebook; ?>" class="socials-item icon-all"><i class="socials-item-icon facebook "></i></a>
						<?php }
						$twitter_profile_url = get_post_meta($post->ID,'twitter_profile_url', true);
						if(!empty($twitter_profile_url)){?>
						<a href="<?php echo $twitter_profile_url; ?>" class="socials-item"><i class="socials-item-icon twitter"></i></a>
						<?php }
						$linkedin_profile_url = get_post_meta($post->ID,'linkedin_profile_url', true);
						if(!empty($linkedin_profile_url)){?>
						<a href="<?php echo $linkedin_profile_url; ?>" class="socials-item"><i class="socials-item-icon linkedin"></i></a>
						<?php }
						 $youtube_profile_url = get_post_meta($post->ID,'youtube_profile_url', true);
						 if(!empty($youtube_profile_url)){?>
						 <a href="<?php echo $youtube_profile_url; ?>" class="socials-item"><i class="socials-item-icon youtube"></i></a>
						 <?php }
						$tumblr_profile_url = get_post_meta($post->ID,'tumblr_profile_url', true);
						if(!empty($tumblr_profile_url)){?>
						<a href="<?php echo $tumblr_profile_url; ?>" class="socials-item"><i class="socials-item-icon tumblr"></i></a>
						<?php }
						$instagram_profile_url = get_post_meta($post->ID,'instagram_profile_url', true);
						if(!empty($instagram_profile_url)){?>
						<a href="<?php echo $instagram_profile_url; ?>" class="socials-item"><i class="socials-item-icon instagram"></i></a>
						<?php }
						$skype_profile_url = get_post_meta($post->ID,'skype_profile_url', true);
						if(!empty($skype_profile_url)){?>
							<a href="<?php echo $skype_profile_url?>"class="socials-item"><i class="socials-item-icon skype"></i></a>
					   <?php }
					   $vimeo_profile_url = get_post_meta($post->ID,'vimeo_profile_url', true);
					   if(!empty($vimeo_profile_url)){?>
						   <a href="<?php echo $vimeo_profile_url?>"class="socials-item"><i class="socials-item-icon vimeo"></i></a>
					  <?php }
						$dribbble_profile_url = get_post_meta($post->ID,'dribbble_profile_url', true);
						if(!empty($dribbble_profile_url)){?>
						<a href="<?php echo $dribbble_profile_url?>"class="socials-item"><i class="socials-item-icon dribbble"></i></a>
						<?php }
						$deviantart_profile_url = get_post_meta($post->ID,'deviantart_profile_url', true);
						if(!empty($deviantart_profile_url)){?>
						<a href="<?php echo $deviantart_profile_url?>"class="socials-item"><i class="socials-item-icon deviantart"></i></a>
						<?php }
						$reddit_profile_url = get_post_meta($post->ID,'reddit_profile_url', true);
						if(!empty($reddit_profile_url)){?>
						<a href="<?php echo $reddit_profile_url?>"class="socials-item"><i class="socials-item-icon reddit"></i></a>
						<?php }
						$flickr_profile_url = get_post_meta($post->ID,'flickr_profile_url', true);
						if(!empty($flickr_profile_url)){?>
						<a href="<?php echo $flickr_profile_url?>"class="socials-item"><i class="socials-item-icon flickr"></i></a>
						<?php }
						?>
						
					  </div>
					</div>
				</div>
			</div>
			<div class="row">
			<div id="post-content" class="post-content col-md-12">
 
				<div id ="para"><?php the_content();?></div>
				<div class="gallery_game">
				<?php $image1 = get_field("image-1");?>
				<?php $picture_img1 = $image1['sizes']['medium'];
				if(!empty($picture_img1)){?>
				<a href="<?php echo get_field('image-1-link'); ?>"><img src="<?php echo $picture_img1; ?>" alt=""></a>
				<?php } else {
				 echo "";
				}?>
				<?php $image2 = get_field("image-2");?>
				<?php $picture_img2 = $image2['sizes']['medium'];
				if(!empty($picture_img2)){?>
				<a href="<?php echo get_field('image-2-link'); ?>"><img src="<?php echo $picture_img2; ?>" alt=""></a>
				<?php } else {
				 echo "";
				}?>
				<?php $image3 = get_field("image-3");?>
				<?php $picture_img3 = $image3['sizes']['medium'];
				if(!empty($picture_img3)){?>
				<a href="<?php echo get_field('image-3-link'); ?>"><img src="<?php echo $picture_img3; ?>" alt=""></a>
				<?php } else {
				 echo "";
				}?>
				<?php $image4 = get_field("image-4");?>
				<?php $picture_img4 = $image4['sizes']['medium'];
				if(!empty($picture_img4)){?>
				<a href="<?php echo get_field('image-4-link'); ?>"><img src="<?php echo $picture_img4; ?>" alt=""></a>
				<?php } else {
				 echo "";
				}?>
				<?php $image5 = get_field("image-5");?>
				<?php $picture_img5 = $image5['sizes']['medium'];
				if(!empty($picture_img5)){?>
				<a href="<?php echo get_field('image-5-link'); ?>"><img src="<?php echo $picture_img5; ?>" alt=""></a>
				<?php } else {
				 echo "";
				}?>
			   <?php $img6 = get_field('image-6');?>
			   <?php $picimg6 = $img6['sizes']['medium']; 
			   if(!empty($picimg6)){?>
			   <a href="<?php echo get_field('image-6-link'); ?>"><img src="<?php echo $picimg6; ?>" alt=""></a>
			   <?php } else {
				 echo "";
				}?>
			   <?php $img7 = get_field('image-7');?>
			   <?php $picimg7 = $img7['sizes']['medium']; 
			   if(!empty($picimg7)){?>
			   <a href="<?php echo get_field('image-7-link'); ?>"><img src="<?php echo $picimg7; ?>" alt=""></a>
			   <?php } else {
				 echo "";
				}?>
			   <?php $img8 = get_field('image-8');?>
			   
			   <?php $picimg8 = $img8['sizes']['medium']; 
			   if(!empty($picimg8)){?>
			   <a href="<?php echo get_field('image-8-link'); ?>"><img src="<?php echo $picimg8; ?>" alt=""></a>
			   <?php } else {
				 echo "";
				}?>
			   <?php $img9 = get_field('image-9');?>
			   <?php $picimg9 = $img9['sizes']['medium']; 
			   if(!empty($picimg9)){?>
			   <a href="<?php echo get_field('image-9-link'); ?>"><img src="<?php echo $picimg9; ?>" alt=""></a>
			   <?php } else {
				 echo "";
				}?>
			   <?php $img10 = get_field('image-10');?>
			   <?php $picimg10 = $img10['sizes']['medium']; 
			   if(!empty($picimg10)){?>
			   <a href="<?php echo get_field('image-10-link'); ?>"><img src="<?php echo $picimg10; ?>" alt=""></a>
			   <?php } else {
				 echo "";
				}?>
				</div>
				
		   </div>
			</div>
		</div>
		<div class="alter">
			<h2><?php the_title();?> Alternatives</h2> 
			<?php get_template_part('games','sidebar'); ?>
			
		</div>
	</div> 
	<div data-css-content="wrapper" data-js-filter="target">
	<?php
	$args = array(
	'post_type' => 'games', 
	'post__not_in' => array( $post->ID )//wordpress exclude current post from loop
	//  'order' => 'ASC',
	);
	$the_query = new WP_Query($args);
	$counter = 0;
	?>

		<?php if($the_query->have_posts() ): ?>
		<?php while($the_query->have_posts()): $counter++; $the_query->the_post();
		$movie_id = get_the_ID();
	
		$url = get_the_permalink();
		$title = get_the_title();
		$thumb = get_the_post_thumbnail_url();
		$content = get_the_content();
		$year = get_post_meta($movie_id,'year',true);
		$rating = get_post_meta($movie_id,'rating',true);
		$runtime = get_post_meta($movie_id,'runtime',true);
		$score = get_post_meta($movie_id,'score',true);

		$taxonomy = 'genre';
		// Get the term IDs assigned to post.
		$post_terms = wp_get_object_terms( $movie_id, $taxonomy, array( 'fields' => 'ids' ) );
		
		// Separator between links.
		$separator = ', ';
		if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
		
			$term_ids = implode( ',' , $post_terms );
		
			$terms = wp_list_categories( array(
				'title_li' => '',
				'style'    => 'none',
				'echo'     => false,
				'taxonomy' => $taxonomy,
				'include'  => $term_ids
			) );
		
			$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
		}

		?>
	
	<div class="block-content-single">
	<div data-css-card="movie-wrapper"class="container" >
		<div data-css-card="movie-thumbnail" class= "post-image col-md-3">
		<?php $game_image = get_field("game-image");?>
							<?php $picture = $game_image['sizes']['thumbnail'];?>
							
							<img src="<?php echo $picture;?>" alt=""> 
		</div>
		<div data-css-card="movie-summary" class="post-text col-md-9">
		<h2 class="site-title"><span class="number"><?= $counter; ?>. </span><a href="<?= $url;?>"><?= $title; ?></a> <span class="date">(<?= $year; ?>)</span></h2>
		<!-- <div data-css-card="movie-meta"> 
			<div class="categories">
			<?php  // echo $terms; ?>
			</div>
		</div> -->
		<p data-css-card="movie-score"><span class="score"><?= $score; ?></span></p>
		<?= $content; ?>
		<div class="show_more"><button  class="cat-btn"><a href="<?= $url;?>">Show Details</a></button></div>
		</div>
	</div>
	</div>
		<?php endwhile; ?>
		<?php endif;?>
	</div>
</div>
<?php
get_footer();
