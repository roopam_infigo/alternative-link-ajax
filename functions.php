<?php 
/*include php files in child theme */
require_once get_template_directory() . '/games_post.php';
require_once get_template_directory() . '/game_meta.php';
require_once get_template_directory() . '/example.php';

/*include php files in child theme
require_once( get_stylesheet_directory(). '/example.php' ); */
function load_scripts() {

/* js include in child theme 
wp_enqueue_script('ajax', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), NULL, true); */
/* js include in parent theme */
wp_enqueue_script('ajax', get_template_directory_uri() . '/js/script.js', array('jquery'), NULL, true);

	wp_localize_script('ajax' , 'wpAjax', 
		array('ajaxUrl' => admin_url('admin-ajax.php'))
	);

}

add_action( 'wp_enqueue_scripts', 'load_scripts' );



